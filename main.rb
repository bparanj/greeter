require_relative './source/person_memory_store'
require_relative './domain/birthday'
require_relative './channel/greeting_console'

# The following code is the client code
pfs = PersonMemoryStore.new
records = pfs.records

records.each do |person|
  month = person.birth_month
  day = person.birth_day
  
  birth_day = BirthDay.new(month, day)

  if birth_day.today?
    message = <<EMAIL_TEXT
    Happy Birthday, Dear #{person.first_name}!
EMAIL_TEXT
    # Client is tied to a specific implementation of sending an email message
    # This needs to change to GreetingEmail.new(message), greeting.send to send email greeting
    greeting = GreetingConsole.new(message, person.email)
    greeting.send
  end
  
end



# This is a domain object
# This object has no dependency on other objects. It is agnostic to storage mechanism
class Person 
  attr_reader :first_name, :email
  
  def initialize(first_name, last_name, date_of_birth, email)
    @first_name = first_name
    @last_name = last_name
    @date_of_birth = date_of_birth
    @email = email
  end
  
  def birth_day
    @date_of_birth[8..9]  
  end
  
  def birth_month
    @date_of_birth[5..6]
  end
end  
require_relative '../domain/person'

#  This class provides in-memory implementation of the data source interface
#  Useful in writing tests
class PersonMemoryStore

  def records
    result = []
    
    person = Person.new('Bugs', 'Bunny', '1982/10/08', 'bbunny@rubyplus.com')
    result << person

    person = Person.new('Daffy', 'Duck', '1975/09/11', 'dduck@rubyplus.com')          
    result << person
    
    result
  end
  
end

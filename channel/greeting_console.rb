# Sending email to the console output is encapsulated within the send interface
class GreetingConsole
  def initialize(message, email)
    @message = message
    @email = email
  end
  
  def send
    p "Sending email to : #{@email}"
    p "Subject : Happy Birthday!"
    p @message    
  end
end

require 'csv'
require_relative '../domain/person'

# This class knows how to parse the CSV file to create Person objects
# The direction of dependency is from PersonFileStore to the domain object
class PersonFileStore
  def initialize(file)
    @file = file
  end

  def records
    result = []
    data = CSV.read(@file, {headers: true}) 
    data.each do |x|
      person = Person.new(x[1], x[0], x[2].strip!, x[3])
      
      result << person
    end    
    result
  end
end

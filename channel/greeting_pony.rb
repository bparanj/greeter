require 'pony'

# Sending a real email using Pony gem
class GreetingPony
  def initialize(message, email)
    @message = message
    @email = email
  end
  
  def send
    Pony.mail(:to => @email, :from => 'admin@rubyplus.com', :subject => 'Happy Birthday!', :body => @message)
  end
end

require 'date'

# This class encapsulates the logic to find out if the birthday is today or not.
# It has no dependency on other objects
class BirthDay
  def initialize(month, day)
    @month = month
    @day = day
  end
  
  def today?
    (@month.to_i == Date.today.month) and (@day.to_i == Date.today.day)
  end
end
